import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import sourceData from "@/data.json";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/destination/:id/:slug",
    name: "destination.details",
    component: () => import("@/views/DestinationDetails.vue"),
    props: (route) => ({ id: parseInt(route.params.id) }),
    children: [
      {
        path: ":experienceSlug",
        name: "experience.show",
        component: () => import("@/views/ExperienceShow.vue"),
        props: (route) => ({ ...route.params, id: parseInt(route.params.id) }),
      },
    ],
		beforeEnter: (to) => {
			const exists = sourceData.destinations.find(
				destination => destination.id === parseInt(to.params.id)
			)

			if(!exists) {
				return {
					name : 'NotFound',
					query : to.query,
					hash : to.hash,
					params : {
						pathMatch : to.path.split('/').slice(1)
					}
				}
			}	
		}
  },
	{
		path: '/:pathMatch(.*)*',
		name : 'NotFound',
		component: () => import("@/components/NotFound.vue"),
	}
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || new Promise((resolve)=>{
      setTimeout(()=> resolve({ top:0, behavior : 'smooth'}), 1000)
    })
  }
});

export default router;
